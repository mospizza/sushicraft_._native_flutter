import 'package:flutter/material.dart';
import 'package:dots_indicator/dots_indicator.dart';

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color(0xFF191919),
      child: SafeArea(
        child: Scaffold(
          body: DefaultTabController(
            length: 13,
            child: NestedScrollView(
                headerSliverBuilder: (BuildContext context,
                    bool innerBoxIsScrolled) {
                  return <Widget>[
                    SliverAppBar(
                      title: Image.asset(
                        'assets/images/log2.png',
                        height: 24,
                      ),
                      expandedHeight: 250,
                      floating: true,
                      pinned: true,
                      flexibleSpace: FlexibleSpaceBar(
                          collapseMode: CollapseMode.none,
                          background: Container(
                            padding: EdgeInsets.only(top: 66),
                            color: Color(0xFF191919),
                            child: Stack(
                                children: [
                                  Container(
                                    color: Color(0xFFF2F2F2),
                                    child: ClipOval(
                                      clipper: CustomRectClipper(),
                                      child: Container(
                                        color: Color(0xFF191919),
                                      ),
                                    ),
                                  ),
                                  Column(
                                    children: [
                                      Container(
                                        height: 132,
                                        color: Color(0xFF191919),
                                        child: Padding(
                                          padding: EdgeInsets.symmetric(
                                              horizontal: 24.0),
                                          child: ClipRRect(
                                            borderRadius: BorderRadius.circular(
                                                14.0),
                                            child: Container(
                                              color: Color(0xFFFF1919),
                                            ),
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(top: 5),
                                        child: DotsIndicator(
                                          dotsCount: 2,
                                          position: 0,
                                          decorator: DotsDecorator(
                                            color: Color(0xFFC4C4C4),
                                            activeColor: Color(0xFFE41E28),
                                          ),
                                        ),
                                      )
                                    ],
                                  )
                                ]
                            ),
                          )
                      ),
                    ),
                    SliverPersistentHeader(
                      delegate: _SliverAppBarDelegate(
                        TabBar(
                            isScrollable: true,
                            labelColor: Color(0xFF8E8E93),
                            indicatorColor: Color(0xFFE41E28),
                            unselectedLabelColor: Color(0xFF8E8E93),
                            tabs: [
                              Tab(text: 'loh'),
                              Tab(text: 'lo123h'),
                              Tab(text: 'l1123oh'),
                              Tab(text: 'loh222'),
                              Tab(text: 'loh2222222'),
                              Tab(text: 'lo123h'),
                              Tab(text: 'lo1h'),
                              Tab(text: 'loh'),
                              Tab(text: 'loh22'),
                              Tab(text: 'loh22'),
                              Tab(text: 'loh12312'),
                              Tab(text: 'lo22h'),
                              Tab(text: 'loh312')
                            ]
                        ),
                      ),
                      pinned: true,
                    )
                  ];
                },
                body: ListView(
                    physics: ClampingScrollPhysics(),
                    children: [
                      Text(
                          '123\n1\n1\n1\n1\n1\n1\n1\n1\n1\n1\n1\n1\n1\n1\n1\n1\n1\n1\n1\n1\n1\n1\n1\n1\n1\n1\n1\n1\n1\n1\n1\n1\n1\n1\n1\n1\n1\n1\n1\n1\n1\n1\n1\n1\n1\n1\n1\n1\n1\n1\n1\n1\n1\n1\n1\n1\n1\n1\n1\n1\n1\n1\n1\n1\n1\n1\n1\n1\n1\n1\n1\n1\n1\n1\n1\n1\n1\n1\n1\n1\n1'),
                    ]
                )
            ),
          ),
        ),
      ),
    );
  }

}



class _SliverAppBarDelegate extends SliverPersistentHeaderDelegate {
  _SliverAppBarDelegate(this._tabBar);

  final TabBar _tabBar;

  @override
  double get minExtent => _tabBar.preferredSize.height;
  @override
  double get maxExtent => _tabBar.preferredSize.height;

  @override
  Widget build(BuildContext context, double shrinkOffset, bool overlapsContent) {
    return new Container(
      child: _tabBar,
    );
  }

  @override
  bool shouldRebuild(_SliverAppBarDelegate oldDelegate) {
    return true;
  }

}

class CustomRectClipper extends CustomClipper<Rect> {

  @override
  Rect getClip(Size size) {
    return Rect.fromLTRB(-size.width * 0.3, 0.0, size.width * 1.3, size.height);
  }

  @override
  bool shouldReclip(CustomClipper<Rect> oldClipper) {
    return true;
  }

}